*** Settings ***
Library  SeleniumLibrary

Test Teardown      Close browser


*** Variables ***
${browser}=  Chrome
${URL}=      http://lexabondrec.ru/
${email}=    LexaBondRec@gmail.com
@{name}=     Алексей    Владимир

*** Test Cases ***
Send message
    Open browser     ${URL}     Chrome
    Set Browser Implicit Wait     5
    Maximize Browser Window
    Input text       id=name    @{name}[0]
    Input text       id=email    ${email}
    Input text       id=message    Тестирование Selenium
    Click button     id=submit

